import { Component, OnInit } from '@angular/core';
import { MycheckService } from '../mycheck.service'
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';

class MyData{
  data : string;
}

@Component({
  selector: 'app-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.css']
})
export class HelloComponent implements OnInit {

  title : string;
  message : string;

  constructor(private client : HttpClient) { }

  ngOnInit(): void {
    this.title = 'Hello-app';
    this.message = 'wait... ';
    setTimeout(() => this.getData(), 5000);
  }

  getData(){
    this.client.get('/assets/data.json').subscribe((result : MyData) =>{
      this.message = 'data: ' + result.data;
    });   
  }
}
