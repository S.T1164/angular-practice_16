import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';

@Component({
  selector: 'app-material',
  templateUrl: './material.component.html',
  styleUrls: ['./material.component.css']
})
export class MaterialComponent implements OnInit {

  message : string;
  myControl : FormGroup;

  constructor() { }

  ngOnInit(): void {
    this.message = 'plese select button.';
  }

  change(v){
    this.message = 'select : " ' + v + ' ".';  
  }
}
